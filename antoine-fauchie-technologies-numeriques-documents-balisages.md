## Les technologies de l'édition numérique sont-elles des documents comme les autres ? (Résumé)

Antoine Fauchié – antoine@quaternum.net

Les documents numériques sont-ils des documents comme les autres (Pédauque et Melot, 2006) ?
Si nous pouvons par exemple admettre qu'un livre numérique homothétique s'intègre dans des espaces documentaires (Epron et Vitali-Rosati, 2018), qu'en est-il de formes non conventionnelles ?
Dynamiques, instables, protéiformes, hybrides, insaisissables, plusieurs exemples récents tendent à interroger l'inscription de nouvelles formes de publications nativement numériques et de leur processus de production dans un écosystème documentaire encore largement conçu pour les formes analogiques, ou numériques homothétiques.
Nous analysons trois systèmes de publication et les formes qu'ils produisent : _Distill_, une revue dont les articles sont gérés comme des programmes informatiques ; Quire, la chaîne de publication de Getty Publications, qui génère des livres multiformes ; Stylo, un éditeur de texte académique qui permet d'écrire avec les outils numériques de notre époque.
Ces trois initiatives rassemblent nombre des spécificités propres aux objets numériques qui nous entourent désormais – comme les logiciels, les bases de données, les applications, les sites web, etc.
Si leurs manifestations – publications imprimées, articles en ligne – semblent répondre à des critères définis (Salaün, 2012), ces technologies de l'édition numérique (Blanc et Haute, 2018) posent une nouvelle questions : doivent-elles être considérées comme des documents ?
En est-il de même pour les artefacts créés à travers elles ?
Ces artefacts peuvent être des informations satellites comme le versionnement, les annotations ou les jeux de données, sont-ils identifiables de la même façon qu'une publication classique ?
Pour formuler une question synthétique : comment ces nouvelles formes et leur système de publication s'intègrent dans un espace documentaire ?
Notre méthodologie consiste en la description et la critique de ces trois systèmes de publication conçus pour le domaine scientifique, emblématiques des transformations numériques à l'œuvre.
Les informations contenues dans les sessions d'enregistrement (ou _commits_) des articles de la revue _Distill_ sont des éléments essentiels pour pouvoir citer ces textes instables.
La chaîne éditoriale de Getty Publications peut être considérée comme un ensemble de documents – ici des programmes – au même titre que les nombreuses formes produites – livre imprimé, livres numériques, jeux de données.
Les formats numériques (Mourat, 2018) gérés par l'éditeur de texte Stylo sont autant d'expressions d'un processus d'écriture en cours qui en facilitent la compréhension.
Nous nous efforçons de ne pas déconnecter ces _nouvelles_ entités (Vandendorpe, 1999) des documents physiques ou numériques plus classiques, confrontant leurs fonctionnements, pointant leur synergie, tout en explorant leurs modalités de conception et de production.


## Bibliographie
Blanc, J., & Haute, L. (2018). Technologies de l’édition numérique. _Sciences du Design_, n° 8(2), 11‑17.

Epron, B., & Vitali-Rosati, M. (2018). _L’édition à l’ère numérique_. Consulté à l’adresse https://www.cairn.info/l-edition-a-l-ere-numerique--9782707199355.htm

Mourat, R. de. (2018). Le design fantomatique des communautés savantes : enjeux phénoménologiques, sociaux et politiques de trois formats de données en usage dans l’édition scientifique contemporaine. _Sciences du Design_, n° 8(2), 34‑44.

Pédauque, R. T., & Melot, M. (2006). _Le document à la lumière du numérique_ (J.-M. Salaün, Éd.). Caen, France: C&F éditions.

Salaün, J.-M. (2012). _Vu, lu, su: les architectes de l’information face à l’oligopole du Web_. Paris, France: La Découverte, impr. 2012.

Vandendorpe, C. (1999). _Du papyrus à l’hypertexte: essai sur les mutations du texte et de la lecture_. Consulté à l’adresse https://vandendorpe.org/papyrus/PapyrusenLigne.pdf

## Antoine Fauchié : Note biographique
Antoine Fauchié (antoine@quaternum.net) est enseignant au département Information-communication de l'IUT2 de Grenoble (Université Grenoble Alpes) et spécialiste indépendant du livre numérique.
Il est également responsable d'une unité d'enseignement du Master Publication numérique de l'École nationale supérieure des sciences de l'information et des bibliothèques.
Engagé dans une pratique de recherche, il débutera un doctorat à l'automne 2019 sous la direction de Marcello Vitali-Rosati à l'Université de Montréal sur la question des processus de publication et des pratiques d'écriture numérique.
Voici une liste de quelques publications et communications :

Blanc, J., & Fauchié, A. (2017, novembre). (Re)Penser les chaînes de publication : soutenabilité et émancipation. Colloque international présenté à Orléans. Orléans.

Fauchié, A. (2017). Le livre web comme objet d’édition ? In _Design et innovation dans la chaîne du livre_. PUF.

Fauchié, A. (2017). Une chaîne de publication inspirée du web. In _Code X: 01-PrePostPrint_. Consulté à l’adresse http://www.editions-hyx.com/fr/code-x

Fauchié, A. (2018). Markdown comme condition d’une norme de l’écriture numérique. _Réél - Virtuel_, (6). Consulté à l’adresse http://www.reel-virtuel.com/numeros/numero6/sentinelles/markdown-condition-ecriture-numerique

Fauchié, A. (2018). Vers un système modulaire de publication: mémoire de Master Publication numérique, Enssib, sous la direction d’Anthony Masure et Marcello Vitali-Rosati. Consulté à l’adresse https://memoire.quaternum.net

Fauchié, A. (2018, avril). Git comme nouvel ingrédient des chaînes de publication. Colloque international présenté à ÉCRIDIL, Montréal, Canada. Consulté à l’adresse http://presentations.quaternum.net/git-comme-nouvel-ingredient-des-chaines-de-publication/

Fauchié, A. (2018, octobre). Vers un système modulaire de publication: éditer avec le numérique. Colloque international présenté à Repenser les humanités numériques / Thinking the Digital Humanities Anew, Montréal, Canada. Consulté à l’adresse https://presentations.quaternum.net/systeme-modulaire-de-publication-crihn/

Fauchié, A. (2019, mars 27). Interopérable, modulaire et multiforme : l’âge d’or de l’édition numérique ? Démonstration par une démarche performative. Consulté 27 mars 2019, à l’adresse DLIS - Digital Libraries & Information Sciences website: https://dlis.hypotheses.org/4381

Fauchié, A., & Parisot, T. (2018). Repenser les chaînes de publication par l’intégration des pratiques du développement logiciel. _Sciences du Design_, n° 8(2), 45‑56.

# Que faire des nouvelles publications ?

## Analyse du contexte

### À propos de la revue
**"Entre balises numériques et balises sémiotiques"**

"[...] susciter une intelligence innovante des questions qui touchent aussi bien au document qu’aux données, au papier qu’au numérique comme aux tensions qui les travaillent."

"_Balisages_ veut questionner les enjeux actuels qui, dans le monde des bibliothèques et de la documentation, nouent les formes du traitement de l’information à celles de la communication."

### À propos de ce numéro
Les objets nativement numériques : transformations et nouveaux enjeux documentaires ?

"Questionner les objets nativement numériques au regard des nouveaux enjeux documentaires qu'ils soulèvent : dans un contexte numérique structurellement instable, se reconfigurant en permanence, le concept de « document » est-il encore opérationnel ?"

"Comment prendre en charge au plan info-documentaire l’instabilité structurelle de ces objets complexes et évolutifs ?"

"confrontation entre objets nativement numériques, hétérogènes, instables, complexes, dynamiques, hybrides et logiques documentaires (collecte, description, classification, partage, diffusion, conservation et archivage...)"

"Les questionnements suivants pourront notamment être abordés :

- Comment qualifier un « objet nativement numérique » ? S’agit-il encore d’un document ?
- Quels enjeux posent ces objets numériques nouveaux pour la pratique documentaire par rapport aux objets documentaires traditionnels ? Ces nouveaux objets échappent-ils à la logique documentaire classique?
- Quelles approches en lien avec des dispositifs techniques rendent possibles leur accès, médiation, diffusions, etc. ?
- Quels acteurs mobilisent-ils ? Dans quels écosystèmes documentaires s’inscrivent-ils ? Quelles nouvelles compétences (documentaires ou autres) nécessitent-ils ?
- Quelles questions juridiques et éthiques soulèvent-ils ?"

## Description de l'intention
**Les nouvelles formes de publication numérique sont-elles des documents comme les autres ?**

Partant d'un objet numérique emprunt du monde analogique et bien matériel, qui semble en cela comme ne présentant pas d'originalité spécifique au-delà de sa _dématérialisation_ (les serveurs eux sont bien physiques, tout comme nos claviers et les câbles qui les lient), qu'est-ce donc que la spécificité de publications numériques d'aujourd'hui ?
Point de départ : trois exemples complémentaires : Distill (versionnement, flux), Getty (), annotation (le texte seul ?)
Qu'est-ce donc que ces nouvelles publications ?
Comment les inscrire dans un écosystème documentaire lui aussi en mouvement ?
Que faire de ces nouvelles publications dans leur ensemble ?

Ne pas limiter à la forme lue à un moment précis, mais également aux autres formes disponibles, aux autres versions (passées, futures, forks), aux paratextes (commits), aux reprises (forks), aux archives possibles, aux versions installées localement sur les téléphones (documentarisation locale), etc.
Comment gérer un objet qui semble documentaire dans l'un de ses aspects mais qui ne l'est pas dans son ensemble (et qui ressemble plus à un programme avec ses dépendances) ?

Un livre numérique au format EPUB est un exemple type de document numérique qui revêt des propriétés très similaires au livre papier : le document est clot dans le sens où il a une dimension définie et arrêtée ; le document est figé dans le sens où une fois téléchargé il ne va pas être modifié ; le document est citable ; etc.
Un livre numérique au format EPUB s'insère parfaitement dans un espace documentaire.
Contre-exemple : une progressive web app, il s'agit cette fois d'un site web avec quelques spécificités, dont la gestion à la façon d'une application, ou le fonctionnement hors ligne. Mais cela peut être un livre.

Supprimer une partie de l'introduction, annoncer plus clairement la question (en quoi les nouvelles formes de publication numérique sont des documents comme les autres qu'il faut traiter différemment), annoncer le terrain et la méthodologie.

### Esquisse de plan
Une introduction permettant de cadrer la définition de "document" et d'environnement documentaire, ainsi que des définitions de "publication".

Dans un premier temps décrire ces objets (description courte, objectifs, analyse, synthèse) avec toute leur complexité et leur potentialité (être positif).
Dans un second temps pointer quelques problèmes rencontrés (évolution, citabilité, archivage, lien (si plusieurs versions), hors connexion (et nécessité de la connexion)), en mettant en regard des publications plus classiques ou traditionnelles (imprimées ou numériques, mais non nativement numérique), et en insistant sur l'écosystème qui n'est pas en mesure de gérer ces nouvelles formes.
Dans un troisième temps esquisser quelques pistes et opportunités.

Les publications numériques semblent être la version dématérialisée de documents analogiques intégrés dans des espaces documentaires classiques, le livre numérique homothétique en est un exemple emblématique (Epron et Vitali-Rosati, 2018), puisque ses propriétés – y compris informationnelles – sont la duplication de celles du livre _papier_.
Nous devons désormais considérer qu'un livre numérique homothétique est une reproduction d'un ouvrage _papier_ sur le plan documentaire, leurs propriétés informationnelles étant très semblables : structure, métadonnées, contenu, fonctionnalités de lecture (Epron et Vitali-Rosati, 2018).


### Titres
Les nouvelles formes de publication numérique sont-elles des documents comme les autres ?

Que faire de ces nouvelles formes de publication numérique ?

Accueillir les nouvelles formes de publication

Inscrire les nouvelles formes de publication dans un écosystème documentaire

Inscription des nouvelles formes de publication

Comprendre les nouvelles formes de publication

Nouvelles formes de publication numérique : habiter les écosystèmes documentaires

### Structure du résumé
Certaines publications numériques rassemblent beaucoup des propriétés des documents nativement numériques : textes, données, programmes, visualisation, etc.
Étudier des nouvelles formes de publication nativement numérique permet de comprendre comment ces objets s'inscrivent dans un environnement documentaire.
